﻿using Boundary;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Presentation.Models;
using System.Threading;
using System.Threading.Tasks;

namespace ShowChat.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MessagesController : 
        ControllerBase
    {
        private readonly ISender _mediator;

        public MessagesController(ISender mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> Get(CancellationToken token = default)
        {
            var request = new GetMessagesRequest();
            var response = await _mediator.Send(request, token);
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] MessageModel model, [FromHeader] string username, CancellationToken token = default)
        {
            var request = new SendMessageRequest(model.Message, username);
            await _mediator.Send(request, token);
            return Accepted();
        }
    }
}
