﻿namespace Presentation.Models
{
    public record MessageModel(string Message);
}
