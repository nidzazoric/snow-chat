﻿using Microsoft.Extensions.DependencyInjection;

namespace Presentation
{
    public static class ComponentInstaller
    {
        public static void AddPresentation(this IServiceCollection services)
        {
            services.AddRouting()
                .AddMvc();
        }
    }
}
