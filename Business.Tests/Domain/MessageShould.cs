using Bogus;
using Business.Domain;
using FluentAssertions;
using System;
using Xunit;

namespace Business.Tests.Domain
{
    public class MessageShould
    {
        [Fact]
        public void BeProperlyConstructed()
        {
            var engine = new Faker();
            var messageContent = engine.Lorem.Lines(2);
            var messageUsername = engine.Internet.UserName();
            var messageTimestamp = engine.Date.RecentOffset().ToUnixTimeSeconds();

            var message = new Message(messageContent,
                messageUsername,
                messageTimestamp);

            message.Content.Should().NotBeNullOrEmpty().And.Be(messageContent);
            message.Username.Should().NotBeNullOrEmpty().And.Be(messageUsername);
            message.Timestamp.Should().BeGreaterThan(0).And.Be(messageTimestamp);
        }

        [Fact]
        public void HaveProperConversionToString()
        {
            var engine = new Faker();
            var messageContent = engine.Lorem.Lines(2);
            var messageUsername = engine.Internet.UserNameUnicode();
            var messageTimestamp = engine.Date.RecentOffset().ToUnixTimeSeconds();

            var message = new Message(messageContent,
                messageUsername,
                messageTimestamp);

            message.ToString().Should().NotBeNullOrEmpty()
                .And.Be($"{messageUsername}: {messageContent} [{DateTimeOffset.FromUnixTimeSeconds(messageTimestamp).DateTime}]");
        }
    }
}
