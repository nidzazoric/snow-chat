﻿using Bogus;
using Business.Domain;

namespace Business.Tests.Generators
{
    public class MessageGenerator :
        Faker<Message>
    {
        public MessageGenerator()
        {
            CustomInstantiator(engine =>
            {
                var messageContent = engine.Lorem.Lines(2);
                var messageUsername = engine.Internet.UserName();
                var messageTimestamp = engine.Date.RecentOffset().ToUnixTimeSeconds();

                return new Message(messageContent, messageUsername, messageTimestamp);
            });
        }
    }
}
