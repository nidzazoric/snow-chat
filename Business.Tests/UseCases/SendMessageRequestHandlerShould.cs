﻿using Bogus;
using Boundary;
using Business.Contracts;
using Business.Domain;
using Business.UseCases;
using FluentAssertions;
using MediatR;
using Moq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Business.Tests.UseCases
{
    public class SendMessageRequestHandlerShould
    {
        private readonly Mock<ICommunicationService> _communicationServiceMock;
        private readonly Mock<IDateTimeProvider> _dateTimeProviderMock;
        private readonly SendMessageRequestHandler _systemUnderTest;

        public SendMessageRequestHandlerShould()
        {
            _communicationServiceMock = new();
            _dateTimeProviderMock = new();
            _systemUnderTest = new(_communicationServiceMock.Object, _dateTimeProviderMock.Object);
        }

        [Fact]
        public void HaveProperInheritance()
        {
            _systemUnderTest.GetType().Should()
                .Implement<IRequestHandler<SendMessageRequest>>();
        }

        [Fact]
        public async Task ReturnAllMessagesSinceExecutionStartFromInMemoryMessagesRepository()
        {
            var engine = new Faker();
            _dateTimeProviderMock.Setup(contractEntry => contractEntry.GetUtcTimestamp())
                .Returns(engine.Date.RecentOffset().ToUnixTimeSeconds());
            _communicationServiceMock.Setup(contractEntry => contractEntry.Send(It.IsAny<Message>()))
                .Returns(Task.CompletedTask);
            var messageContent = engine.Lorem.Lines(2);
            var messageUsername = engine.Internet.UserName();
            var request = new SendMessageRequest(messageContent, messageUsername);

            var response = await _systemUnderTest.Handle(request, CancellationToken.None);

            response.Should().Be(Unit.Value);
        }
    }
}
