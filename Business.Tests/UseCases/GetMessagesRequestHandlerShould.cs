﻿using Boundary;
using Business.Contracts;
using Business.Domain;
using Business.Tests.Generators;
using Business.UseCases;
using FluentAssertions;
using MediatR;
using Moq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Business.Tests.UseCases
{
    public class GetMessagesRequestHandlerShould
    {
        private readonly Mock<IRepository<Message>> _messageRepositoryMock;
        private readonly GetMessagesRequestHandler _systemUnderTest;

        public GetMessagesRequestHandlerShould()
        {
            _messageRepositoryMock = new();
            _systemUnderTest = new(_messageRepositoryMock.Object);
        }

        [Fact]
        public void HaveProperInheritance()
        {
            _systemUnderTest.GetType().Should()
                .Implement<IRequestHandler<GetMessagesRequest, IReadOnlyCollection<MessageResponse>>>();
        }

        [Fact]
        public async Task ReturnAllMessagesSinceExecutionStartFromInMemoryMessagesRepository()
        {
            var messagesCollection = new MessageGenerator().Generate(10);
            _messageRepositoryMock.Setup(contractEntry => contractEntry.Read())
                .Returns(messagesCollection);
            var request = new GetMessagesRequest();

            var response = await _systemUnderTest.Handle(request, CancellationToken.None);

            response.Should().NotBeEmpty()
                .And.HaveCount(messagesCollection.Count)
                .And.AllBeAssignableTo<MessageResponse>();
        }
    }
}
