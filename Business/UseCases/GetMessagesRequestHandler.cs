﻿using Boundary;
using Business.Contracts;
using Business.Domain;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Business.UseCases
{
    public class GetMessagesRequestHandler :
        IRequestHandler<GetMessagesRequest, IReadOnlyCollection<MessageResponse>>
    {
        private readonly IRepository<Message> _store;

        public GetMessagesRequestHandler(IRepository<Message> store)
        {
            _store = store;
        }

        public Task<IReadOnlyCollection<MessageResponse>> Handle(GetMessagesRequest request, CancellationToken cancellationToken)
        {
            var messageCollection = _store.Read()
                .Select(messageEntry => new MessageResponse(messageEntry.Content, messageEntry.Username, messageEntry.Timestamp))
                .ToList();

            return Task.FromResult(messageCollection as IReadOnlyCollection<MessageResponse>);
        }
    }
}
