﻿using Boundary;
using Business.Contracts;
using Business.Domain;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Business.UseCases
{
    public class SendMessageRequestHandler :
        IRequestHandler<SendMessageRequest>
    {
        private readonly ICommunicationService _communicationService;
        private readonly IDateTimeProvider _dateTimeProvider;

        public SendMessageRequestHandler(ICommunicationService communicationService,
            IDateTimeProvider dateTimeProvider)
        {
            _communicationService = communicationService;
            _dateTimeProvider = dateTimeProvider;
        }

        public async Task<Unit> Handle(SendMessageRequest request, CancellationToken cancellationToken)
        {
            var sentMessage = new Message(request.Message, request.Username, _dateTimeProvider.GetUtcTimestamp());
            await _communicationService.Send(sentMessage);
            return Unit.Value;
        }
    }
}
