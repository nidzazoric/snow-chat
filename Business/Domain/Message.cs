﻿using System;

namespace Business.Domain
{
    public class Message
    {
        public string Content { get; }
        public string Username { get; }
        public long Timestamp { get; }

        public Message(string content, string username, long timestamp)
        {
            Content = content;
            Username = username;
            Timestamp = timestamp;
        }

        public override string ToString()
        {
            return $"{Username}: {Content} [{DateTimeOffset.FromUnixTimeSeconds(Timestamp).DateTime}]";
        }
    }
}
