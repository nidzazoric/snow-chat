﻿namespace Business.Contracts
{
    public interface IDateTimeProvider
    {
        long GetUtcTimestamp();
    }
}
