﻿using Business.Domain;
using System.Threading.Tasks;

namespace Business.Contracts
{
    public interface ICommunicationService
    {
        Task Send(Message message);
    }
}
