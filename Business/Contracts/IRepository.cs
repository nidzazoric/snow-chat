﻿using System.Collections.Generic;

namespace Business.Contracts
{
    public interface IRepository<TModel>
    {
        void Add(TModel message);
        IReadOnlyCollection<TModel> Read();
    }
}
