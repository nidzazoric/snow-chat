# Snow Chat

Snow chat presentation software powered by
- net 5.0
- nats
- docker

To run project
- pointing from root of project location run > docker-compose up
- first client send message EP [POST] https://localhost:12459/api/messages -H Username:{some-username} -H Content-Type:application/json -d { "message": "somemessage" }
- second client send message EP [POST] https://localhost:12469/api/messages -H Username:{some-username} -H Content-Type:application/json -d { "message": "somemessage" }
- to fetch messages [GET] https://localhost:12459/api/messages
