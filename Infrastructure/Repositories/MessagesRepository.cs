﻿using Business.Contracts;
using Business.Domain;
using System.Collections.Generic;

namespace Infrastructure.Repositories
{
    public class MessagesRepository :
        IRepository<Message>
    {
        private readonly List<Message> _messages;

        public MessagesRepository()
        {
            _messages = new();
        }

        public void Add(Message message)
        {
            _messages.Add(message);
        }

        public IReadOnlyCollection<Message> Read()
        {
            return _messages;
        }
    }
}
