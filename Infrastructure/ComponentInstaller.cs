﻿using Business.Contracts;
using Business.Domain;
using Infrastructure.Background;
using Infrastructure.Providers;
using Infrastructure.Repositories;
using Infrastructure.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;
using MyNatsClient;
using System;

namespace Infrastructure
{
    public static class ComponentInstaller
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {
            if (services is null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            services.TryAddTransient<INatsClient>(serviceProviderEntry =>
            {
                var connectionOptionsFactory = serviceProviderEntry.GetRequiredService<IOptions<CommunicationPlatformOptions>>();
                var connection = new ConnectionInfo(connectionOptionsFactory.Value.Url.Host);
                return new NatsClient(connection);
            });
            services.TryAddScoped<ICommunicationService, CommunicationService>();
            services.AddHostedService<CommunicationHostedService>();
            return services;
        }

        public static IServiceCollection AddInfrastructure(this IServiceCollection services, Action<CommunicationPlatformOptions> configureOptions)
        {
            if (services is null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            if(configureOptions is null)
            {
                throw new ArgumentNullException(nameof(configureOptions));
            }

            services.Configure(configureOptions);
            services.AddInfrastructure();
            return services;
        }

        public static IServiceCollection AddPersistence(this IServiceCollection services)
        {
            services.TryAddSingleton<IRepository<Message>, MessagesRepository>();
            return services;
        }

        public static IServiceCollection AddProviders(this IServiceCollection services)
        {
            services.TryAddTransient<IDateTimeProvider, DateTimeProvider>();
            return services;
        }
    }
}
