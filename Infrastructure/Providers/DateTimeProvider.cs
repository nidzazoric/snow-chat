﻿using Business.Contracts;
using System;

namespace Infrastructure.Providers
{
    public class DateTimeProvider :
        IDateTimeProvider
    {
        public long GetUtcTimestamp()
        {
            return DateTimeOffset.UtcNow.ToUnixTimeSeconds();
        }
    }
}
