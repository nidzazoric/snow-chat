﻿using System;

namespace Infrastructure
{
    public class CommunicationPlatformOptions
    {
        public Uri Url { get; set; }
        public string Subject { get; set; }
    }
}
