﻿using Business.Contracts;
using Business.Domain;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using MyNatsClient;
using MyNatsClient.Encodings.Protobuf;
using MyNatsClient.Ops;
using MyNatsClient.Rx;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Background
{
    public class CommunicationHostedService :
        IHostedService
    {
        private readonly IRepository<Message> _repository;
        private INatsClient _client;
        private ISubscription _subscription;
        private readonly CommunicationPlatformOptions _options;

        public CommunicationHostedService(IOptions<CommunicationPlatformOptions> optionsFactory,
            IRepository<Message> repository,
            INatsClient client)
        {
            _repository = repository;
            _client = client;
            _options = optionsFactory.Value;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await _client.ConnectAsync();
            _subscription = await _client.SubAsync(_options.Subject, stream => stream.SubscribeSafe(OnMessageReceived));
        }

        private void OnMessageReceived(MsgOp payload)
        {
            var message = payload.FromProtobuf<Message>();
            _repository.Add(message);
            Console.WriteLine(message);
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await _client.UnsubAsync(_subscription);
            _subscription.Dispose();
            _client.Disconnect();
        }
    }
}
