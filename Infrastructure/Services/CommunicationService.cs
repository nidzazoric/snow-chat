﻿using Business.Contracts;
using Business.Domain;
using Microsoft.Extensions.Options;
using MyNatsClient;
using MyNatsClient.Encodings.Protobuf;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class CommunicationService :
        ICommunicationService
    {
        private readonly CommunicationPlatformOptions _options;
        private readonly INatsClient _client;

        public CommunicationService(IOptions<CommunicationPlatformOptions> optionsFactory,
            INatsClient client)
        {
            _options = optionsFactory.Value;
            _client = client;
        }

        public async Task Send(Message message)
        {
            await _client.ConnectAsync();
            await _client.PubAsProtobufAsync(_options.Subject, message, null);
            _client.Disconnect();
        }
    }
}
