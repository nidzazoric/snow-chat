﻿using MediatR;
using System.Collections.Generic;

namespace Boundary
{
    public record GetMessagesRequest() : IRequest<IReadOnlyCollection<MessageResponse>>;
    public record SendMessageRequest(string Message, string Username) : IRequest;
}
