﻿namespace Boundary
{
    public record MessageResponse(string Content, string Username, long Timestamp);
}
