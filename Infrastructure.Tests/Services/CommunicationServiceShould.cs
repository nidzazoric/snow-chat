using Business.Contracts;
using FluentAssertions;
using Infrastructure.Services;
using Infrastructure.Tests.Generators;
using Microsoft.Extensions.Options;
using Moq;
using MyNatsClient;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Infrastructure.Tests
{
    public class CommunicationServiceShould
    {
        private readonly Mock<INatsClient> _clientMock;
        private readonly Mock<IOptions<CommunicationPlatformOptions>> _optionsFactoryMock;
        private readonly CommunicationService _systemUnderTest;

        public CommunicationServiceShould()
        {
            _clientMock = new();
            _optionsFactoryMock = new();
            _optionsFactoryMock.Setup(contractEntry => contractEntry.Value)
                .Returns(new CommunicationPlatformOptions
                {
                    Url = new Uri("nats://demo.nats.com:4422"),
                    Subject = "test"
                });

            _systemUnderTest = new(_optionsFactoryMock.Object, _clientMock.Object);
        }

        [Fact]
        public void HaveProperInheritence()
        {
            _systemUnderTest.GetType().Should()
                .Implement<ICommunicationService>();
        }

        [Fact]
        public async Task SendMessageToConnectedNatsClient()
        {
            var message = new MessageGenerator().Generate();

            await _systemUnderTest.Send(message);

            _clientMock.Verify(contractEntry => contractEntry.ConnectAsync());
            _clientMock.Verify(contractEntry => contractEntry.PubAsync(It.IsAny<string>(), It.IsAny<ReadOnlyMemory<byte>>(), null));
            _clientMock.Verify(contractEntry => contractEntry.Disconnect());
        }
    }
}
