﻿using Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace App.Components.Infrastructure
{
    public class CommunicationPlatformOptionsConfigurator :
        IConfigureOptions<CommunicationPlatformOptions>
    {
        private const string _communicationConfigurationSectionName = "Communication";
        private readonly IConfiguration _configuration;

        public CommunicationPlatformOptionsConfigurator(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void Configure(CommunicationPlatformOptions options)
        {
            _configuration.GetSection(_communicationConfigurationSectionName).Bind(options);
        }
    }
}
