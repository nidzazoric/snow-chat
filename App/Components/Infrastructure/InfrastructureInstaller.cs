﻿using App.Base;
using Infrastructure;
using Microsoft.Extensions.DependencyInjection;

namespace App.Components.Infrastructure
{
    public class InfrastructureInstaller :
        IInstaller
    {
        public void InstallServices(IServiceCollection services)
        {
            services.ConfigureOptions<CommunicationPlatformOptionsConfigurator>();
            
            services.AddInfrastructure();
            services.AddPersistence();
            services.AddProviders();
        }
    }
}
