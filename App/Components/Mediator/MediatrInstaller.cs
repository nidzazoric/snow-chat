﻿using App.Base;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace App.Components.Mediator
{
    public class MediatrInstaller :
        IInstaller
    {
        public void InstallServices(IServiceCollection services)
        {
            services.AddMediatR(typeof(Business.AssemblyReference).Assembly);
        }
    }
}
