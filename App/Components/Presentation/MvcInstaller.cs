﻿using App.Base;
using Microsoft.Extensions.DependencyInjection;
using Presentation;

namespace App.Components.Presentation
{
    public class MvcInstaller :
        IInstaller
    {
        public void InstallServices(IServiceCollection services)
        {
            services.AddPresentation();
        }
    }
}
