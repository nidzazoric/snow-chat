﻿using Microsoft.Extensions.DependencyInjection;

namespace App.Base
{
    internal interface IInstaller
    {
        void InstallServices(IServiceCollection services);
    }
}
