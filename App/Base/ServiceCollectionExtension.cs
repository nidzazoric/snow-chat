﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Reflection;

namespace App.Base
{
    internal static class ServiceCollectionExtension
    {
        internal static void AddInstallersFrom(this IServiceCollection services, Assembly scanAssembly)
        {
            var installers = scanAssembly.ExportedTypes
                .Where(IsConstructableInstallerInstance)
                .Select(Activator.CreateInstance)
                .Cast<IInstaller>()
                .ToList();

            installers.ForEach(installerEntry => installerEntry.InstallServices(services));
        }

        private static bool IsConstructableInstallerInstance(Type type)
        {
            return typeof(IInstaller).IsAssignableFrom(type) && !type.IsInterface && !type.IsAbstract;
        }
    }
}
