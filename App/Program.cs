using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System.Reflection;

namespace App
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args)
                .Build()
                .Run();
        }

        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration(WithDefaultAppConfiguration)
                .ConfigureWebHostDefaults(WithDefaultWebHostConfiguration);

        private static void WithDefaultAppConfiguration(HostBuilderContext hostContext, IConfigurationBuilder configuration)
        {
            var environment = hostContext.HostingEnvironment;

            configuration.SetBasePath(environment.ContentRootPath);
            configuration.AddJsonFile("configuration.application.json", false);
            configuration.AddJsonFile($"configuration.application.{environment.EnvironmentName.ToLowerInvariant()}.json", true);

            if (!environment.IsProduction())
            {
                configuration.AddUserSecrets(Assembly.GetExecutingAssembly());
            }

            configuration.AddEnvironmentVariables();
        }

        private static void WithDefaultWebHostConfiguration(IWebHostBuilder webBuilder)
        {
            webBuilder.UseStartup<Startup>();
        }
    }
}
